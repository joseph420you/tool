package com.product;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by Joseph on 2021/3/6.
 */
public class ConfigPropertiesLoader {
    private final Properties properties;

    public ConfigPropertiesLoader() {
        this.properties = new Properties();
    }

    /**
     * 載入resources資料夾下*.properties檔，並初始化實體物件
     * 所有Bean需定義在src/main/java/下
     *
     * @return 設定檔實體集合
     */
    public List<?> loadConfigs(String configFileName) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try (InputStream stream = loader.getResourceAsStream(configFileName)) {
            properties.load(stream);

            Map<String/*full class name*/, List<String/*fields*/>> classAndValues = getClassAndFieldNameDetails(properties);

            return getInstances(classAndValues);
        } catch (IOException | NoSuchFieldException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return Collections.EMPTY_LIST;
    }

    private Map<String/*full class name*/, List<String/*fields*/>> getClassAndFieldNameDetails(Properties properties) {
        Map<String/*full class name*/, List<String/*fields*/>> classAndFields = new HashMap<>();
        String fieldName;
        String packageName;
        List<String> fields;

        for (String configKey : properties.stringPropertyNames()) {
            packageName = getPackageName(configKey);
            fieldName = getFieldName(configKey);

            fields = classAndFields.getOrDefault(packageName, new ArrayList<>());

            if (fields.contains(fieldName)) {
                throw new IllegalArgumentException("duplicated field definitions:" + configKey);
            }

            fields.add(fieldName);

            classAndFields.putIfAbsent(packageName, fields);
        }

        return classAndFields;
    }

    private String getPackageName(String configKey) {
        String[] tmp = configKey.split("\\.");
        tmp[tmp.length - 1] = ""; //去掉field name，保留package name

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i <= tmp.length - 2; ++i) {
            if (i == tmp.length - 2) {
                sb.append(tmp[i]);
            } else {
                sb.append(tmp[i]).append(".");
            }
        }

        return sb.toString();
    }

    private String getFieldName(String configKey) {
        String[] tmp = configKey.split("\\.");
        return tmp[tmp.length - 1];
    }

    private List<Object> getInstances(Map<String/*full class name*/, List<String/*fields*/>> classAndFieldValues) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        Object obj;
        List<Object> instances = new ArrayList<>();
        List<String> fieldsName;

        for (String packageClassName : classAndFieldValues.keySet()) {
            fieldsName = classAndFieldValues.get(packageClassName);
            obj = getConfigInstance(fieldsName, packageClassName);
            instances.add(obj);
        }

        return instances;
    }

    private Object getConfigInstance(List<String> fields, String packageClassName) throws IllegalAccessException, InstantiationException, NoSuchFieldException, ClassNotFoundException {
        Class<?> clazz = Class.forName(packageClassName);
        Object obj = clazz.newInstance();
        Field fieldInstance;
        String fieldValue;

        for (String field : fields) {
            fieldInstance = clazz.getDeclaredField(field);
            fieldValue = properties.getProperty(packageClassName + "." + field);

            setFieldValue(fieldInstance, fieldValue, obj);
        }

        return obj;
    }

    private void setFieldValue(Field field, String fieldValue, Object obj) throws IllegalAccessException {
        field.setAccessible(true);

        Class<?> fieldType = field.getType();

        if (fieldType == Byte.class || fieldType == byte.class) {
            field.setByte(obj, Byte.parseByte(fieldValue));
        } else if (fieldType == Character.class || fieldType == char.class) {
            field.setChar(obj, fieldValue.charAt(0));
        } else if (fieldType == Short.class || fieldType == short.class) {
            field.setShort(obj, Short.parseShort(fieldValue));
        } else if (fieldType == Boolean.class || fieldType == boolean.class) {
            field.setBoolean(obj, Boolean.parseBoolean(fieldValue));
        } else if (fieldType == Integer.class || fieldType == int.class) {
            field.setInt(obj, Integer.parseInt(fieldValue));
        } else if (fieldType == Long.class || fieldType == long.class) {
            field.setLong(obj, Long.parseLong(fieldValue));
        } else if (fieldType == Float.class || fieldType == float.class) {
            field.setFloat(obj, Float.parseFloat(fieldValue));
        } else if (fieldType == Double.class || fieldType == double.class) {
            field.setDouble(obj, Double.parseDouble(fieldValue));
        } else if (fieldType == String.class) {
            field.set(obj, fieldValue);
        } else {
            throw new AssertionError("Undefine data type parser:" + fieldType);
        }
    }
}
