package com.product.ExcelParser;

import com.google.gson.Gson;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;

/**
 * Created by Joseph on 2020/7/26.
 */
public class CSingleSheetParser<T> {
    private static Logger logger = Logger.getLogger("ExcelParser");
    private Class<T> objectType;
    private final int startRowIndex; //若欄位名稱含在檔案內，則可以跳過
    private Map<String/*物件屬性名稱*/, Class<?>/*物件屬行型別*/> fieldTypeInfo;
    private Map<String/*物件屬性名稱*/, Object/*物件屬性資料*/> realData;
    private Gson encodeUtil;
    private String sheetName;

    /**
     * 建構子
     *
     * @param objectType    欲轉回的pojo物件型別
     * @param startRowIndex 讀取excel起始列索引値(0開始)
     */
    public CSingleSheetParser(Class<T> objectType, int startRowIndex) {
        this.objectType = objectType;
        this.startRowIndex = startRowIndex;
        fieldTypeInfo = new LinkedHashMap<>();
        clearData();
        encodeUtil = new Gson();
    }

    private void clearData() {
        realData = new LinkedHashMap<>();
    }

    /**
     * 設定對應剖析物件屬性名稱與型別
     *
     * @param fieldName 物件屬性名稱
     * @param tClass    物件屬性型別
     * @return this
     */
    public CSingleSheetParser<T> addObjectField(String fieldName, Class<?> tClass) {
        fieldTypeInfo.put(fieldName, tClass);
        return this;
    }

    /**
     * 剖析成實體物件
     *
     * @param sheet excel工作表物件
     * @return 實體物件集合
     * @throws DataFormatException 表格欄位或行數不正確
     */
    public List<T> parse(Sheet sheet) throws DataFormatException {
        sheetName = sheet.getSheetName();
        logger.info("Number of rows in sheet[" + sheetName + "] " + sheet.getLastRowNum());

        List<T> objs = new ArrayList<>();

        for (int i = startRowIndex; i < sheet.getLastRowNum(); ++i) {
            objs.add(getRealObject(getFieldValues(sheet.getRow(i))));
        }

        return objs;
    }

    private List<String> getFieldValues(Row row) {
        List<String> values = new ArrayList<>();
        for (Cell cell : row) {
            if (cell == null || cell.toString().equals("")) {
                throw new NullPointerException("Field data is empty.");
            }

            values.add(cell.toString());
        }

        return values;
    }

    private T getRealObject(List<String> fieldValues) throws DataFormatException {
        if (fieldValues.size() != fieldTypeInfo.size()) {
            throw new DataFormatException("Sheet [" + sheetName + "] field data either missing or losing column.");
        }

        int index = 0;
        for (String filedName : fieldTypeInfo.keySet()) {
            fillUpAsRealData(filedName, fieldValues.get(index), fieldTypeInfo.get(filedName));
            ++index;
        }

        String data = encodeUtil.toJson(realData);
        return encodeUtil.fromJson(data, objectType);
    }

    private void fillUpAsRealData(String fieldName, String data, Class<?> tClass) {
        realData.put(fieldName, getValueObject(data, tClass));
    }

    private Object getValueObject(String data, Class<?> tClass) {
        if (Integer.class == tClass) {
            return Float.valueOf(data).intValue();
        } else if (Long.class == tClass) {
            return Float.valueOf(data).longValue();
        } else if (Float.class == tClass) {
            return Float.parseFloat(data);
        } else if (Double.class == tClass) {
            return Double.parseDouble(data);
        } else if (String.class == tClass) {
            return data;
        } else {
            return encodeUtil.fromJson(data, tClass);
        }
    }
}