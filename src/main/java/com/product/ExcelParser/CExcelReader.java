package com.product.ExcelParser;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Joseph on 2020/7/26.
 */
public class CExcelReader {
    private static final String EXCEL_XLS = "xls";
    private static final String EXCEL_XLSX = "xlsx";

    /**
     * 取得excel物件
     * [備註]
     * 第三方poi_4.1.2 API會讓剖析的excel資料筆數少最後一筆
     * 建議來源excel檔加上最後一列假資料以便正常讀取所有完整資料
     *
     * @param sourceFile 來源檔案
     * @return
     * @throws IOException
     */
    public static Workbook getWorkbook(File sourceFile) throws IOException {
        FileInputStream in = new FileInputStream(sourceFile);

        if (sourceFile.getName().endsWith(EXCEL_XLS)) {
            return new HSSFWorkbook(in);
        } else if (sourceFile.getName().endsWith(EXCEL_XLSX)) {
            return new XSSFWorkbook(in);
        } else {
            throw new FileNotFoundException("undefined file type: " + sourceFile.getName());
        }
    }
}